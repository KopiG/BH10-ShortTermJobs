/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh10.project.shorttermjobs.dao;

import com.bh10.project.shorttermjobs.entity.HardSkillEntity;
import com.bh10.project.shorttermjobs.entity.JobApplicantDataEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Maxim
 */
@Stateless
public class HardSkillDAO {

    @PersistenceContext
    EntityManager em;

    public List<HardSkillEntity> findHardSkillByJobApplicant(JobApplicantDataEntity jobApplicantDataEntity) {
        return (List<HardSkillEntity>) em.createQuery("select h from HardSkillEntity h where h.jobApplicantData = :id")
                .setParameter("id", jobApplicantDataEntity)
                .getResultList();
    }
    
    public HardSkillEntity findHardSkillById(Integer id) {
        return (HardSkillEntity) em.createQuery("select h from HardSkillEntity h where h.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }
    
    public void updateHardSkill(HardSkillEntity entity) {
        em.merge(entity);
    }
    
    public void addNewHardSkill(HardSkillEntity entity) {
        em.persist(entity);
    }
    
    public void deleteHardSKill(HardSkillEntity entity) {
        em.remove(entity);
    }
}
