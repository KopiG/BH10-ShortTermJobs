/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh10.project.shorttermjobs.mapper;

import com.bh10.project.shorttermjobs.dto.HardSkillDto;
import com.bh10.project.shorttermjobs.entity.HardSkillEntity;

/**
 *
 * @author Maxim
 */
public final class HardSkillMapper {

    public static HardSkillDto toDto(HardSkillEntity entity) {
        HardSkillDto dto = new HardSkillDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setLevel(entity.getLevel());
        return dto;
    }
}
