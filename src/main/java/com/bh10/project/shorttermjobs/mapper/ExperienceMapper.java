/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bh10.project.shorttermjobs.mapper;

import com.bh10.project.shorttermjobs.dto.ExperienceDto;
import com.bh10.project.shorttermjobs.entity.ExperienceEntity;

/**
 *
 * @author Maxim
 */
public final class ExperienceMapper {
    
    public static ExperienceDto toDto(ExperienceEntity entity) {
        ExperienceDto dto = new ExperienceDto();
        dto.setId(entity.getId());
        dto.setCompany(entity.getCompany());
        dto.setTerm(entity.getTerm());
        dto.setPosition(entity.getPosition());
        dto.setDescription(entity.getDescription());
        return dto;
    }
}
