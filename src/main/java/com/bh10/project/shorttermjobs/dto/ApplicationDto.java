/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bh10.project.shorttermjobs.dto;

/**
 *
 * @author Maxim
 */
public class ApplicationDto {

    private Integer id;
    private StjUserDto user;
    private AdvertisementDto advertisement;

    public ApplicationDto() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public StjUserDto getUser() {
        return user;
    }

    public void setUser(StjUserDto user) {
        this.user = user;
    }

    public AdvertisementDto getAdvertisement() {
        return advertisement;
    }

    public void setAdvertisement(AdvertisementDto advertisement) {
        this.advertisement = advertisement;
    }

    @Override
    public String toString() {
        return "ApplicationDto{" + "id=" + id + ", user=" + user + ", advertisement=" + advertisement + '}';
    }
    
    
    
    
}
