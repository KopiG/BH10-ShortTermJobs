/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh10.project.shorttermjobs.exception;

/**
 *
 * @author csubi
 */
public class UserAlreadyReportedThisException extends Exception{

    @Override
    public String getMessage() {
        return "This user already reported this advertisement";
    }
    
}
