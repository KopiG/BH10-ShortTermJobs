
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/advertisement.css"/>" />
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/myads.css"/>" />
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/queries/advertisement_query.css"/>"/>
        <title>MyAdvertisements</title>
    </head>
    <body>
        <jsp:include page="navbar.jsp"></jsp:include>
        <c:forEach items="${myads}" var="item">
            <div class="advert-container">
                <div class="advert">
                    <div class="jobtype">
                        <img src="<c:url value="resources/images/icons/job_icon.png"/>" alt="Job Icon"><p>${item.jobType}</p>
                    </div>
                    <div class="location">
                        <img src="<c:url value="resources/images/icons/location_icon.png"/>" alt="Location Icon"><p>${item.location}</p>
                    </div>
                    <div class="date">
                        <img src="<c:url value="resources/images/icons/calendar_icon.png"/>" alt="Calendar Icon"><p>${item.date}</p>
                    </div>
                    <div class="contact">
                        <img src="<c:url value="resources/images/icons/contact_icon.png"/>" alt="Contact Icon">
                        <ul>
                            <li>${item.stjUserDto.name}</li>
                            <li>${item.stjUserDto.email}</li>
                        </ul>
                    </div>
                    <div class="description">
                        <p>${item.description}</p>
                    </div>
                    <div class="delete-btn">
                        <form action="myAdvertisements" method="POST">
                            <input type="hidden" name="advertisementId" value="${item.id}">
                            <button type="submit" name="deleteAdvert" value="delete"><img src="<c:url value="resources/images/icons/delete_icon.png"/>" alt="delete_icon"></button>
                        </form>
                    </div>
                </div>
                <div class="show-applicants-btn">
                    <div onclick="expandList(${item.id})" id="forward-icon-btn${item.id}" class="forward-icon-btn">
                        <img src="<c:url value="resources/images/icons/forward_icon.png"/>" alt="forward_icon">
                    </div>
                    <div style="display: none;" onclick="collapseList(${item.id})" id="backward-icon-btn${item.id}" class="backward-icon-btn">
                        <img src="<c:url value="resources/images/icons/backward_icon.png"/>" alt="backward_icon">
                    </div>
                </div>
                <div id="applicants-list${item.id}" class="applicants-list">
                    <p id="advertisement-id" style="display: none;">${item.id}</p>
                    <ul>
                        <c:forEach items="${item.applications}" var="item">
                            <a href="profile?query=${item.user.id}"><li>${item.user.name}</li></a>
                                </c:forEach>
                    </ul>
                </div>
            </div>
            <br>
        </c:forEach>
        <script type="text/javascript" src="<c:url value="resources/js/myads.js"/>"></script>
    </body>
</html>
