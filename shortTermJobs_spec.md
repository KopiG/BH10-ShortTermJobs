    @ChilliBeans
    SHORT-TERM JOBS
A short-term jobs webes applikációval a célunk egy olyan alkalmazás létrehozása, 
amiben szolgáltatást nyújtunk, mind magánszemélyek, mind pedig vállalkozások  
számára, hogy rövidtávú munkát hirdethessenek meg, valamint a felhasználók 
jelentkezhessenek ezen meghirdetett alkalmi munkákra.
Az applikáció egy kapcsolatteremtési funkciót is ellát (email), megteremti 
a kommunikációs csatornát a hirdető (munkáltató) valamint a kliensek 
(munkavállalók) között. 
A meghirdetett munkák böngészését elősegítő funkció a hirdetések kategorizálása
(vállalkozások vagy magánszemélyek hirdető/kereső). Szűrési lehetőségek: specifikusabb
munkakörök, terület és kereső mező.
A weblap böngészése nincsen regisztrációhoz kötve, így mindenki láthatja a hirdetéseket,
viszont ha bármilyen tevékenységet szeretne folytatni az oldalon (hirdetés/munkavállalás),
ahhoz kötelező az érvényes felhasználóval való bejelentkezés. Amennyiben nem rendelkezik az
ügyfél érvényes felhasználóval, létrehozhat egyet a regisztrálás funkcióval. 
Minden regisztrált ügyfélnek lesz egy ‘profil’-ja amiben szerepelnie kell a teljes nevének,
email címének, valamint önéletrajzának. E három kötelező dolgon kívül lesz lehetősége  még
profilképet feltölteni, leírást adni magáról, egyéb személyes adatot megadni (születési idő,
hely, telefonszám, egyéb elérhetőségek), valamint iskolai végzettségét és tapasztalatait is 
beírhatja, így amennyiben jelentkezik egy meghirdetett munkára, akkor a munkáltató egyből 
láthatja a jelentkező képességeit az önéletrajz megnyitása nélkül is.
A felhasználók szabadon hirdethetnek bármely kategóriában, egy űrlap kitöltésével melyben az
alkalmi pozíció részleteit leírhatják, a dátum, a helyszín, a munkabér, az elvárások és a
munka jellegének megadásával. A felhasználó megtekintheti a ‘saját hirdetéseim’ menüpont
alatt a feladott hirdetéseit. 
Az jelentkezésnél mindkét fél láthatja egymás elérhetőségeit, így fel tudják venni a 
kapcsolatot egymással.
Amennyiben a két fél között a munkakapcsolat megvalósult, úgy lehetőségük van egymás 
profiljának az értékelésére melyet a nyilvánosság is megismerhet, ezáltal a legközelebbi
hirdetés/munkavállalás során a többi felhasználónak segítséget nyújt a megfelelő jelentkező 
vagy hirdető kiválasztásához.
A nem megfelelő hirdetések szűrésére, a felhasználók jelenthetik a hirdetéseket amennyiben
úgy gondolják, hogy az nem oda való. A jelentéseket az admin felhasználók megkapják és
eldönthetik, hogy törlik-e vagy sem.
Az admin továbbá törölhet felhasználókat ha nem tartják be a szabályzatot valamint lehetősége 
van több adminisztrátor profil létrehozására is.

Technológiák: 
1.	- Adatbázis -> MySql
2.	- Alkalmazás Szerver -> Payara
3.	- JPA 
4.	- JTA
5.	- EJB
6.	- JavaScript, HTML, CSS
7.	- Bootstrap, JQuery
8.	- JavaMail API
9.	- Hibernate
